use std::collections::HashMap;
use std::fmt::Debug;
use noise::{Seedable, Point2, NoiseFn, Perlin};
use crate::world::registry::BlockRegistry;

pub mod registry;

#[derive(Debug)]
pub enum WorldError {
    BlockNotRegistered(BlockId),
    BlockCoordinatesOutOfBounds,
    ChunkNotLoaded,
}

pub type BlockId = u32;

#[derive(Clone)]
pub struct Blocks(pub Box<[BlockId]>); // len = 4096 = 16 * 16 * 16

impl Debug for Blocks {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> Result<(), std::fmt::Error> {
        write!(f, "<blocks>")
    }
}

#[derive(Debug, Clone)]
pub struct Chunk {
    pub blocks: Blocks,
}

impl Chunk {
    pub fn generate(perlin: &Perlin, registry: &BlockRegistry, x: i32, y: i32, z: i32) -> Chunk {
        let nothing_id = registry.get_id("crelax", "nothing").unwrap();
        let stone_id = registry.get_id("crelax", "stone").unwrap();

        let mut blocks = vec![nothing_id; 4096];

        for bx in 0..16 {
            for bz in 0..16 {
                let h = perlin.get(Point2::from([(x as f64) + (bx as f64) / 16.0, (z as f64) + (bz as f64) / 16.0])) / 2.0 + 0.5;
                let h = (h * 16.0) as usize;

                for by in 0..h {
                    blocks[bz * 256 + by * 16 + bx] = stone_id;
                }
            }
        }

        Chunk {
            blocks: Blocks(blocks.into_boxed_slice())
        }
    }

    pub fn empty() -> Chunk {
        let blocks = vec![0; 4096];

        Chunk {
            blocks: Blocks(blocks.into_boxed_slice())
        }
    }
}

impl std::ops::Index<(usize, usize, usize)> for Chunk {
    type Output = BlockId;

    fn index(&self, (x, y, z): (usize, usize, usize)) -> &Self::Output {
        &self.blocks.0[z * 256 + y * 16 + x]
    }
}

// Region =
// 32 * 32 Chunk Column =
// 16 Chunk =
// 16 * 16 * 16 Block

pub struct ChunkColumn(Box<[Chunk]>); // len = 16

pub struct Region(Box<[ChunkColumn]>); // len = 1024 = 32 * 32

impl Region {
    pub fn empty() -> Region {
        Region((0..1024).map(|_| ChunkColumn((0..16).map(|_| Chunk::empty()).collect::<Vec<_>>().into_boxed_slice())).collect::<Vec<_>>().into_boxed_slice())
    }
}

pub struct World {
    pub regions: HashMap<(i32, i32), Region>,
    pub perlin: Perlin,
}

impl World {
    pub fn new() -> World {
        World {
            regions: HashMap::new(),
            perlin: Perlin::new(),
        }
    }

    pub fn get(&self, x: i32, y: i32, z: i32) -> Option<&Chunk> {
        Some(&self.regions.get(&(x / 32, z / 32))?.0[((((z % 32) + 32) % 32) * 32 + (((x % 32) + 32) % 32)) as usize].0[y as usize])
    }

    pub fn set(&mut self, x: i32, y: i32, z: i32, chunk: Chunk) {
        self.regions.entry((x / 32, z / 32)).or_insert(Region::empty()).0[((((z % 32) + 32) % 32) * 32 + (((x % 32) + 32) % 32)) as usize].0[y as usize] = chunk;
    }

    pub fn generate_chunk_column(&mut self, registry: &BlockRegistry, x: i32, z: i32) -> ChunkColumn {
        generate_chunk_column(&self.perlin, registry, x, z)
    }
}

pub fn generate_chunk_column(perlin: &Perlin, registry: &BlockRegistry, x: i32, z: i32) -> ChunkColumn {
    ChunkColumn((0..16).map(|y| Chunk::generate(perlin, registry, x, y, z)).collect::<Vec<_>>().into_boxed_slice())
}