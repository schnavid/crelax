use crate::world::BlockId;
use std::collections::{BTreeMap, HashMap};
use crate::graphics::registry::TextureId;

#[derive(Clone, Debug, PartialEq, Eq)]
pub enum BlockMaterial {
    Nothing,
    Stone,
    Wood,
    Dirt,
    Grass,
}

impl From<u8> for BlockMaterial {
    fn from(n: u8) -> Self {
        match n {
            1 => BlockMaterial::Stone,
            2 => BlockMaterial::Wood,
            3 => BlockMaterial::Dirt,
            4 => BlockMaterial::Grass,
            _ => BlockMaterial::Nothing,
        }
    }
}

impl Into<u8> for BlockMaterial {
    fn into(self) -> u8 {
        match self {
            BlockMaterial::Nothing => 0,
            BlockMaterial::Stone => 1,
            BlockMaterial::Wood => 2,
            BlockMaterial::Dirt => 3,
            BlockMaterial::Grass => 4,
        }
    }
}

#[derive(Debug, Clone)]
pub enum BlockTexturing {
    All(TextureId),
    Pillar {
        top: TextureId,
        side: TextureId,
        bottom: TextureId,
    },
    None,
}

#[derive(Clone, Debug)]
pub struct BlockInfo {
    pub module: String,
    pub name: String,
    pub material: BlockMaterial,
    pub texture: BlockTexturing,
    pub opaque: bool,
}

#[derive(Debug)]
pub struct BlockRegistry {
    pub info: Vec<BlockInfo>,
    pub map: HashMap<(String, String), BlockId>,
}

impl BlockRegistry {
    pub fn new() -> BlockRegistry {
        BlockRegistry {
            info: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn register(&mut self, info: BlockInfo) -> BlockId {
        let id = self.info.len() as u32;
        self.info.push(info.clone());
        self.map.insert((info.module, info.name), id);
        id
    }

    pub fn get(&self, blk: BlockId) -> Option<BlockInfo> {
        self.info.get(blk as usize).map(|b| b.clone())
    }

    pub fn get_id<S: Into<String>>(&self, module: S, name: S) -> Option<BlockId> {
        self.map.get(&(module.into(), name.into())).map(|&b| b)
    }
}
