use crate::error::VulkanError;
use crate::graphics::traits::ChunkRenderer;
use crate::world::{Chunk, WorldError, World};
use crate::graphics::vulkan::{Vertex, VulkanContext};
use crate::error::Error;
use crate::world::registry::{BlockMaterial, BlockRegistry};
use std::sync::Arc;
use vulkano::buffer::{BufferUsage, ImmutableBuffer};
use vulkano::sync::GpuFuture;

pub struct VulkanChunkRenderer {
    pub vertex_buffer: Option<Arc<ImmutableBuffer<[Vertex]>>>,
    pub index_buffer: Option<Arc<ImmutableBuffer<[u32]>>>,
}

impl VulkanChunkRenderer {
    fn generate_vertices(
        &self,
        world: &World,
        registry: &BlockRegistry,
        cx: i32,
        cy: i32,
        cz: i32,
    ) -> Result<(Vec<Vertex>, Vec<u32>), WorldError> {
        let mut vertices = Vec::new();
        let mut indices = Vec::new();
        let chunk = world.get(cx, cy, cz).ok_or(WorldError::ChunkNotLoaded)?;

        for x in 0..16 {
            for y in 0..16 {
                for z in 0..16 {
                    let blk = chunk[(x, y, z)].clone();
                    let info = registry
                        .get(blk)
                        .ok_or(WorldError::BlockNotRegistered(blk.clone()))?;

                    if info.material != BlockMaterial::Nothing {
                        let front = if z == 15 {
                            world
                                .get(cx, cy, cz + 1)
                                .map(|c| registry
                                    .get(c[(x, y, 0)])
                                    .ok_or(WorldError::BlockNotRegistered(chunk[(x, y, 0)].clone()))
                                )
                                .map(|b| b.map(|b| !b.opaque))
                                .unwrap_or(Ok(true))?
                        } else {
                            !registry
                                .get(chunk[(x, y, z + 1)])
                                .ok_or(WorldError::BlockNotRegistered(chunk[(x, y, z + 1)].clone()))?
                                .opaque
                        };

                        let right = if x == 15 {
                            world
                                .get(cx + 1, cy, cz)
                                .map(|c| registry
                                    .get(c[(0, y, z)])
                                    .ok_or(WorldError::BlockNotRegistered(chunk[(0, y, z)].clone()))
                                )
                                .map(|b| b.map(|b| !b.opaque))
                                .unwrap_or(Ok(true))?
                        } else {
                            !registry
                                .get(chunk[(x + 1, y, z)])
                                .ok_or(WorldError::BlockNotRegistered(chunk[(x + 1, y, z)].clone()))?
                                .opaque
                        };

                        let back = if z == 0 {
                            world
                                .get(cx, cy, cz - 1)
                                .map(|c| registry
                                    .get(c[(x, y, 15)])
                                    .ok_or(WorldError::BlockNotRegistered(chunk[(x, y, 15)].clone()))
                                )
                                .map(|b| b.map(|b| !b.opaque))
                                .unwrap_or(Ok(true))?
                        } else {
                            !registry
                                .get(chunk[(x, y, z - 1)])
                                .ok_or(WorldError::BlockNotRegistered(chunk[(x, y, z - 1)].clone()))?
                                .opaque
                        };

                        let left = if x == 0 {
                            world
                                .get(cx - 1, cy, cz)
                                .map(|c| registry
                                    .get(c[(15, y, z)])
                                    .ok_or(WorldError::BlockNotRegistered(chunk[(15, y, z)].clone()))
                                )
                                .map(|b| b.map(|b| !b.opaque))
                                .unwrap_or(Ok(true))?
                        } else {
                            !registry
                                .get(chunk[(x - 1, y, z)])
                                .ok_or(WorldError::BlockNotRegistered(chunk[(x - 1, y, z)].clone()))?
                                .opaque
                        };

                        let top = y == 15
                            || !registry
                            .get(chunk[(x, y + 1, z)])
                            .ok_or(WorldError::BlockNotRegistered(chunk[(x, y + 1, z)].clone()))?
                            .opaque;
                        let bottom = y == 0
                            || !registry
                            .get(chunk[(x, y - 1, z)])
                            .ok_or(WorldError::BlockNotRegistered(chunk[(x, y - 1, z)].clone()))?
                            .opaque;

                        let (bv, bi) = block_vertices(
                            front, right, back, left, top, bottom, (cx * 16 + x as i32) as f32, (cy * 16 + y as i32) as f32, (cz * 16 + z as i32) as f32,
                        );

                        indices.extend(bi.iter().map(|i| i + vertices.len() as u32));
                        vertices.extend(bv.iter().cloned());
                    }
                }
            }
        }

        Ok((vertices, indices))
    }

    fn generate_buffers(
        &mut self,
        vertices: Vec<Vertex>,
        indices: Vec<u32>,
        context: &VulkanContext,
    ) -> Result<(), VulkanError> {
        let (vb, vb_fut) = ImmutableBuffer::from_iter(
            vertices.iter().cloned(),
            BufferUsage::vertex_buffer(),
            context.queue.clone(),
        )?;

        let (ib, ib_fut) = ImmutableBuffer::from_iter(
            indices.iter().cloned(),
            BufferUsage::index_buffer(),
            context.queue.clone(),
        )?;

        self.vertex_buffer = Some(vb);
        self.index_buffer = Some(ib);

        let _fut = vb_fut.join(ib_fut);

        Ok(())
    }
}

impl ChunkRenderer<VulkanContext> for VulkanChunkRenderer {
    fn new(_: &VulkanContext) -> Self {
        VulkanChunkRenderer {
            vertex_buffer: None,
            index_buffer: None,
        }
    }

    fn generate(
        &mut self,
        world: &World,
        registry: &BlockRegistry,
        context: &VulkanContext,
        x: i32,
        y: i32,
        z: i32,
    ) -> Result<(), Error> {
        let (vertices, indices) = self.generate_vertices(world, registry, x, y, z)?;

        Ok(self.generate_buffers(vertices, indices, context)?)
    }
}

fn block_vertices(
    front: bool,
    right: bool,
    back: bool,
    left: bool,
    top: bool,
    bottom: bool,
    x: f32,
    y: f32,
    z: f32,
) -> (Vec<Vertex>, Vec<u32>) {
    let mut vertices = Vec::new();
    let mut indices = Vec::new();

    fn add_side(len: usize, indices: &mut Vec<u32>) {
        let len = len as u32;
        indices.extend_from_slice(&[len, len + 1, len + 2, len + 2, len + 3, len]);
    }

    if front {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 0.0, y + 0.0, z + 1.0], [0.0, 0.0, 1.0], [0.0, 1.0]),
            Vertex::new([x + 1.0, y + 0.0, z + 1.0], [0.0, 0.0, 1.0], [1.0, 1.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 1.0], [0.0, 0.0, 1.0], [1.0, 0.0]),
            Vertex::new([x + 0.0, y + 1.0, z + 1.0], [0.0, 0.0, 1.0], [0.0, 0.0]),
        ]);
    }

    if right {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 1.0, y + 0.0, z + 1.0], [1.0, 0.0, 0.0], [0.0, 1.0]),
            Vertex::new([x + 1.0, y + 0.0, z + 0.0], [1.0, 0.0, 0.0], [1.0, 1.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 0.0], [1.0, 0.0, 0.0], [1.0, 0.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 1.0], [1.0, 0.0, 0.0], [0.0, 0.0]),
        ])
    }

    if back {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 1.0, y + 0.0, z + 0.0], [0.0, 0.0, -1.0], [0.0, 1.0]),
            Vertex::new([x + 0.0, y + 0.0, z + 0.0], [0.0, 0.0, -1.0], [1.0, 1.0]),
            Vertex::new([x + 0.0, y + 1.0, z + 0.0], [0.0, 0.0, -1.0], [1.0, 0.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 0.0], [0.0, 0.0, -1.0], [0.0, 0.0]),
        ])
    }

    if left {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 0.0, y + 0.0, z + 0.0], [-1.0, 0.0, 0.0], [0.0, 1.0]),
            Vertex::new([x + 0.0, y + 0.0, z + 1.0], [-1.0, 0.0, 0.0], [1.0, 1.0]),
            Vertex::new([x + 0.0, y + 1.0, z + 1.0], [-1.0, 0.0, 0.0], [1.0, 0.0]),
            Vertex::new([x + 0.0, y + 1.0, z + 0.0], [-1.0, 0.0, 0.0], [0.0, 0.0]),
        ])
    }

    if top {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 0.0, y + 1.0, z + 1.0], [0.0, 1.0, 0.0], [0.0, 1.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 1.0], [0.0, 1.0, 0.0], [1.0, 1.0]),
            Vertex::new([x + 1.0, y + 1.0, z + 0.0], [0.0, 1.0, 0.0], [1.0, 0.0]),
            Vertex::new([x + 0.0, y + 1.0, z + 0.0], [0.0, 1.0, 0.0], [0.0, 0.0]),
        ])
    }

    if bottom {
        add_side(vertices.len(), &mut indices);

        vertices.extend_from_slice(&[
            Vertex::new([x + 1.0, y + 0.0, z + 1.0], [0.0, -1.0, 0.0], [0.0, 1.0]),
            Vertex::new([x + 0.0, y + 0.0, z + 1.0], [0.0, -1.0, 0.0], [1.0, 1.0]),
            Vertex::new([x + 0.0, y + 0.0, z + 0.0], [0.0, -1.0, 0.0], [1.0, 0.0]),
            Vertex::new([x + 1.0, y + 0.0, z + 0.0], [0.0, -1.0, 0.0], [0.0, 0.0]),
        ])
    }

    (vertices, indices)
}