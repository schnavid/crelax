use cgmath::Angle;
use cgmath::Deg;
use cgmath::InnerSpace;
use cgmath::Matrix4;
use cgmath::Point3;
use cgmath::Vector3;
use crate::graphics::camera::{Camera, FirstPersonCamera};
use cgmath::num_traits::real::Real;

pub struct VulkanFirstPersonCamera {
    pos: Point3<f32>,
    forward: Vector3<f32>,
    upwards: Vector3<f32>,
    yaw: f32,
    pitch: f32,
}

impl VulkanFirstPersonCamera {
    pub fn new() -> VulkanFirstPersonCamera {
        VulkanFirstPersonCamera {
            pos: [0.0, 0.0, 0.0].into(),
            forward: [0.0, 0.0, -1.0].into(),
            upwards: [0.0, 1.0, 0.0].into(),
            yaw: 180.0,
            pitch: 0.0,
        }
    }
}

impl Camera for VulkanFirstPersonCamera {
    fn matrix(&self) -> Matrix4<f32> {
        Matrix4::look_at_dir(self.pos, self.forward, [0.0, 1.0, 0.0].into())
    }
}

impl FirstPersonCamera for VulkanFirstPersonCamera {
    fn handle_mouse(&mut self, x: f32, y: f32) {
        let sensitivity = 0.5;
        self.yaw += x * sensitivity;
        self.pitch += y * sensitivity;

        if self.pitch > 89.0 {
            self.pitch = 89.0;
        }
        if self.pitch < -89.0 {
            self.pitch = -89.0;
        }

        let forward: Vector3<f32> = [
            Deg(self.yaw).cos() * Deg(self.pitch).cos(),
            Deg(self.pitch).sin(),
            Deg(self.yaw).sin() * Deg(self.pitch).cos(),
        ]
        .into();

        self.forward = forward.normalize();
    }

    fn move_forward(&mut self, by: f32) {
        self.pos += [self.forward.x * by, 0.0, self.forward.z * by].into();
    }

    fn move_upwards(&mut self, by: f32) {
        self.pos += [0.0, by, 0.0].into();
    }

    fn move_sideways(&mut self, by: f32) {
        self.pos += self.forward.cross(self.upwards) * by;
    }
}
