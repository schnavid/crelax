use vulkano::buffer::{BufferUsage, CpuBufferPool};
use vulkano::command_buffer::{AutoCommandBufferBuilder, DynamicState};
use vulkano::descriptor::descriptor_set::PersistentDescriptorSet;
use vulkano::device::{Device, DeviceExtensions, Features, Queue};
use vulkano::format::Format;
use vulkano::framebuffer::{Framebuffer, FramebufferAbstract, RenderPassAbstract, Subpass};
use vulkano::image::{AttachmentImage, Dimensions, ImmutableImage, SwapchainImage};
use vulkano::instance::{Instance, PhysicalDevice};
use vulkano::pipeline::viewport::Viewport;
use vulkano::pipeline::{GraphicsPipeline, GraphicsPipelineAbstract};
use vulkano::swapchain::{
    self, AcquireError, PresentMode, Surface, SurfaceTransform, Swapchain, SwapchainCreationError,
};
use vulkano::sync::{self, FlushError, GpuFuture};

use vulkano_win::VkSurfaceBuild;

use winit::{
    dpi::{LogicalPosition, LogicalSize},
    DeviceEvent, ElementState, Event, EventsLoop, KeyboardInput, VirtualKeyCode, Window,
    WindowBuilder, WindowEvent,
};

use std::iter;
use std::sync::Arc;

use cgmath::{Deg, Matrix4, SquareMatrix};

use crate::graphics::traits::{ChunkRenderer, Context};
use image::ImageFormat;
use vulkano::sampler::{Filter, MipmapMode, Sampler, SamplerAddressMode};

use crate::graphics::vulkan::camera::VulkanFirstPersonCamera;
use crate::graphics::vulkan::chunk_rendering::VulkanChunkRenderer;
use crate::error::VulkanError;
use crate::graphics::camera::{Camera, FirstPersonCamera};
use crate::world::registry::{
    BlockInfo, BlockMaterial, BlockRegistry, BlockTexturing,
};
use crate::world::{BlockId, Chunk, World};
use std::cell::RefCell;
use std::collections::HashMap;
use crate::graphics::registry::{TextureRegistry, TextureInfo};
use crate::protocol::Packet;
use noise::{Perlin, Seedable};

pub mod camera;
pub mod chunk_rendering;
mod fs;
mod vs;

#[derive(Default, Debug, Clone)]
pub struct Vertex {
    position: [f32; 3],
    normal: [f32; 3],
    tex_coord: [f32; 2],
}

impl Vertex {
    pub fn new(position: [f32; 3], normal: [f32; 3], tex_coord: [f32; 2]) -> Vertex {
        Vertex {
            position,
            normal,
            tex_coord,
        }
    }
}

vulkano::impl_vertex!(Vertex, position, normal, tex_coord);

pub struct VulkanContext {
    events_loop: EventsLoop,
    surface: Arc<Surface<Window>>,
    win_title: String,
    dimensions: [u32; 2],
    swapchain: Arc<Swapchain<winit::Window>>,
    images: Vec<Arc<SwapchainImage<winit::Window>>>,
    device: Arc<Device>,
    render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
    pipeline: Arc<dyn GraphicsPipelineAbstract + Send + Sync>,
    framebuffers: Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
    vs: vs::Shader,
    fs: fs::Shader,
    texture: Arc<ImmutableImage<Format>>,
    sampler: Arc<Sampler>,
    queue: Arc<Queue>,
    dynamic_state: DynamicState,
}

impl VulkanContext {
    pub fn new<T: Into<String> + Clone>(
        title: T,
        win_width: u32,
        win_height: u32,
    ) -> Result<VulkanContext, VulkanError> {
        let instance = {
            let extensions = vulkano_win::required_extensions();
            Instance::new(None, &extensions, None)?
        };

        let events_loop = EventsLoop::new();
        let surface = WindowBuilder::new()
            .with_title(title.clone())
            .with_dimensions(LogicalSize::new(win_width as f64, win_height as f64))
            .build_vk_surface(&events_loop, instance.clone())?;

        let physical = PhysicalDevice::enumerate(&instance)
            .next()
            .ok_or(VulkanError::NoPhysicalDeviceAvailable)?;

        for family in physical.queue_families() {
            println!(
                "Found a queue family with {:?} queue(s)",
                family.queues_count()
            );
        }

        let queue_family = physical
            .queue_families()
            .find(|&q| q.supports_graphics())
            .ok_or(VulkanError::NoGraphicalQueueFamily)?;

        let (device, mut queues) = {
            let device_ext = vulkano::device::DeviceExtensions {
                khr_swapchain: true,
                ..DeviceExtensions::none()
            };

            Device::new(
                physical,
                &Features::none(),
                &device_ext,
                [(queue_family, 0.5)].iter().cloned(),
            )
                .expect("failed to create device")
        };

        let queue = queues.next().unwrap();

        let dimensions = if let Some(dimensions) = surface.window().get_inner_size() {
            let dimensions: (u32, u32) = dimensions
                .to_physical(surface.window().get_hidpi_factor())
                .into();
            Ok([dimensions.0, dimensions.1])
        } else {
            Err(VulkanError::WindowNoLongerExists)
        }?;

        let (swapchain, images) = {
            let caps = surface.capabilities(physical)?;
            let usage = caps.supported_usage_flags;
            let alpha = caps.supported_composite_alpha.iter().next().unwrap();
            let format = caps.supported_formats[0].0;

            Swapchain::new(
                device.clone(),
                surface.clone(),
                caps.min_image_count,
                format,
                dimensions,
                1,
                usage,
                &queue,
                SurfaceTransform::Identity,
                alpha,
                PresentMode::Fifo,
                true,
                None,
            )?
        };

        let (texture, mut tex_future) = {
            let image =
                image::load_from_memory_with_format(include_bytes!("stone.png"), ImageFormat::PNG)
                    .unwrap()
                    .to_rgba();
            let image_data = image.into_raw().clone();

            ImmutableImage::from_iter(
                image_data.iter().cloned(),
                Dimensions::Dim2d {
                    width: 16,
                    height: 16,
                },
                Format::R8G8B8A8Srgb,
                queue.clone(),
            )
                .unwrap()
        };

        let sampler = Sampler::new(
            device.clone(),
            Filter::Nearest,
            Filter::Nearest,
            MipmapMode::Nearest,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            SamplerAddressMode::Repeat,
            0.0,
            1.0,
            0.0,
            0.0,
        )?;

        let vs = vs::Shader::load(device.clone()).unwrap();
        let fs = fs::Shader::load(device.clone()).unwrap();

        // At this point, OpenGL initialization would be finished. However in Vulkan it is not. OpenGL
        // implicitly does a lot of computation whenever you draw. In Vulkan, you have to do all this
        // manually.

        // The next step is to create a *render pass*, which is an object that describes where the
        // output of the graphics pipeline will go. It describes the layout of the images
        // where the colors, depth and/or stencil information will be written.
        let render_pass = Arc::new(
            vulkano::single_pass_renderpass!(
                device.clone(),
                attachments: {
                    color: {
                        load: Clear,
                        store: Store,
                        format: swapchain.format(),
                        samples: 1,
                    },
                    depth: {
                        load: Clear,
                        store: DontCare,
                        format: Format::D16Unorm,
                        samples: 1,
                    }
                },
                pass: {
                    color: [color],
                    depth_stencil: {depth}
                }
            )
                .unwrap(),
        );

        let dynamic_state = DynamicState {
            line_width: None,
            viewports: None,
            scissors: None,
        };

        let (pipeline, framebuffers) =
            window_size_dependent_setup(device.clone(), &vs, &fs, &images, render_pass.clone())?;

        tex_future.cleanup_finished();

        Ok(VulkanContext {
            events_loop,
            surface,
            win_title: title.into(),
            dimensions,
            swapchain,
            images,
            device,
            render_pass,
            pipeline,
            framebuffers,
            vs,
            fs,
            texture,
            sampler,
            queue,
            dynamic_state,
        })
    }

    pub fn center_cursor(&self) {
        let size = self.surface.window().get_inner_size().unwrap();
        self.surface
            .window()
            .set_cursor_position(LogicalPosition::new(size.width / 2.0, size.height / 2.0))
            .unwrap();
    }

    pub fn run(&mut self, rx: std::sync::mpsc::Receiver<Packet>) -> Result<(), VulkanError> {
        let mut recreate_swapchain = false;
        let mut previous_frame_end = Box::new(sync::now(self.device.clone())) as Box<dyn GpuFuture>;

        let mut cam = VulkanFirstPersonCamera::new();

        let pressed_keys = RefCell::new(HashMap::new());

        let mut world = World::new();

        let mut block_registry = BlockRegistry::new();
        let mut texture_registry = TextureRegistry::new();

        let stone_texture = texture_registry.register(
            TextureInfo {
                module: String::from("crelax"),
                name: String::from("stone"),
                left: 0,
                top: 0,
                width: 16,
                height: 16,
            }
        );

        let nothing_id = block_registry.register(
            BlockInfo {
                module: String::from("crelax"),
                name: String::from("nothing"),
                material: BlockMaterial::Nothing,
                texture: BlockTexturing::None,
                opaque: false,
            },
        );

        let stone_id = block_registry.register(
            BlockInfo {
                module: String::from("crelax"),
                name: String::from("stone"),
                material: BlockMaterial::Stone,
                texture: BlockTexturing::All(stone_texture),
                opaque: true,
            },
        );

        let mut perlin = Perlin::new();
        perlin.set_seed(0);

        world.set(0, 0, 0, Chunk::generate(&perlin, &block_registry, 0, 0, 0));
        world.set(1, 0, 0, Chunk::generate(&perlin, &block_registry, 1, 0, 0));
        world.set(1, 0, 1, Chunk::generate(&perlin, &block_registry, 1, 0, 1));
        world.set(0, 0, 1, Chunk::generate(&perlin, &block_registry, 0, 0, 1));

        let mut chunk_renderer1 = VulkanChunkRenderer::new(self);
        chunk_renderer1.generate(&world, &block_registry, self, 0, 0, 0).unwrap();
        let mut chunk_renderer2 = VulkanChunkRenderer::new(self);
        chunk_renderer2.generate(&world, &block_registry, self, 1, 0, 0).unwrap();
        let mut chunk_renderer3 = VulkanChunkRenderer::new(self);
        chunk_renderer3.generate(&world, &block_registry, self, 0, 0, 1).unwrap();
        let mut chunk_renderer4 = VulkanChunkRenderer::new(self);
        chunk_renderer4.generate(&world, &block_registry, self, 1, 0, 1).unwrap();

        loop {
            previous_frame_end.cleanup_finished();

            while let Ok(packet) = rx.try_recv() {
                println!("{:#?}", packet);
            }

            if recreate_swapchain {
                self.dimensions = if let Some(dimensions) = self.surface.window().get_inner_size() {
                    let dimensions: (u32, u32) = dimensions
                        .to_physical(self.surface.window().get_hidpi_factor())
                        .into();
                    [dimensions.0, dimensions.1]
                } else {
                    break;
                };

                let (new_swapchain, new_images) =
                    match self.swapchain.recreate_with_dimension(self.dimensions) {
                        Err(SwapchainCreationError::UnsupportedDimensions) => continue,
                        a => a?,
                    };

                self.swapchain = new_swapchain;
                self.images = new_images;
                let (new_pipeline, new_framebuffers) = window_size_dependent_setup(
                    self.device.clone(),
                    &self.vs,
                    &self.fs,
                    &self.images,
                    self.render_pass.clone(),
                )?;
                self.pipeline = new_pipeline;
                self.framebuffers = new_framebuffers;

                recreate_swapchain = false;
            }

            // Before we can draw on the output, we have to *acquire* an image from the swapchain. If
            // no image is available (which happens if you submit draw commands too quickly), then the
            // function will block.
            // This operation returns the index of the image that we are allowed to draw upon.
            //
            // This function can block if no image is available. The parameter is an optional timeout
            // after which the function call will return an error.
            let (image_num, acquire_future) =
                match swapchain::acquire_next_image(self.swapchain.clone(), None) {
                    Ok(r) => r,
                    Err(AcquireError::OutOfDate) => {
                        recreate_swapchain = true;
                        continue;
                    }
                    Err(err) => panic!("{:?}", err),
                };

            // Specify the color to clear the framebuffer with i.e. blue
            let clear_values = vec![[0.0, 0.0, 0.0, 1.0].into(), 1f32.into()];

            let uniform_buffer =
                CpuBufferPool::<vs::ty::Data>::new(self.device.clone(), BufferUsage::all());

            let uniform_buffer_subbuffer = {
                let aspect_ratio = self.dimensions[0] as f32 / self.dimensions[1] as f32;
                let mut proj = cgmath::perspective(Deg(45.0), aspect_ratio, 0.01, 100.0);
                proj[1][1] = -proj[1][1];
                let view = cam.matrix();

                let uniform_data = vs::ty::Data {
                    world: Matrix4::identity().into(),
                    view: view.into(),
                    proj: proj.into(),
                };

                uniform_buffer.next(uniform_data).unwrap()
            };

            let set = Arc::new(
                PersistentDescriptorSet::start(self.pipeline.clone(), 0)
                    .add_buffer(uniform_buffer_subbuffer)?
                    .add_sampled_image(self.texture.clone(), self.sampler.clone())?
                    .build()
                    .unwrap(),
            );

            let mut builder = AutoCommandBufferBuilder::primary_one_time_submit(
                self.device.clone(),
                self.queue.family(),
            )
                .unwrap()
                // Before we can draw, we have to *enter a render pass*. There are two methods to do
                // this: `draw_inline` and `draw_secondary`. The latter is a bit more advanced and is
                // not covered here.
                //
                // The third parameter builds the list of values to clear the attachments with. The API
                // is similar to the list of attachments when building the framebuffers, except that
                // only the attachments that use `load: Clear` appear in the list.
                .begin_render_pass(self.framebuffers[image_num].clone(), false, clear_values)
                .unwrap();

            builder = builder.draw_indexed(
                self.pipeline.clone(),
                &self.dynamic_state,
                vec![chunk_renderer1.vertex_buffer.clone().unwrap()],
                chunk_renderer1.index_buffer.clone().unwrap(),
                set.clone(),
                (),
            )?;

            builder = builder.draw_indexed(
                self.pipeline.clone(),
                &self.dynamic_state,
                vec![chunk_renderer2.vertex_buffer.clone().unwrap()],
                chunk_renderer2.index_buffer.clone().unwrap(),
                set.clone(),
                (),
            )?;

            builder = builder.draw_indexed(
                self.pipeline.clone(),
                &self.dynamic_state,
                vec![chunk_renderer3.vertex_buffer.clone().unwrap()],
                chunk_renderer3.index_buffer.clone().unwrap(),
                set.clone(),
                (),
            )?;

            builder = builder.draw_indexed(
                self.pipeline.clone(),
                &self.dynamic_state,
                vec![chunk_renderer4.vertex_buffer.clone().unwrap()],
                chunk_renderer4.index_buffer.clone().unwrap(),
                set.clone(),
                (),
            )?;

            // We leave the render pass by calling `draw_end`. Note that if we had multiple
            // subpasses we could have called `next_inline` (or `next_secondary`) to jump to the
            // next subpass.
            let command_buffer = builder
                .end_render_pass()
                .unwrap()
                // Finish building the command buffer by calling `build`.
                .build()?;

            let future = previous_frame_end
                .join(acquire_future)
                .then_execute(self.queue.clone(), command_buffer)?
                // This function does not actually present the image immediately. Instead it submits a
                // present command at the end of the queue. This means that it will only be presented once
                // the GPU has finished executing the command buffer that draws the triangle.
                .then_swapchain_present(self.queue.clone(), self.swapchain.clone(), image_num)
                .then_signal_fence_and_flush();

            match future {
                Ok(future) => {
                    previous_frame_end = Box::new(future) as Box<_>;
                }
                Err(FlushError::OutOfDate) => {
                    recreate_swapchain = true;
                    previous_frame_end = Box::new(sync::now(self.device.clone())) as Box<_>;
                }
                Err(e) => {
                    println!("{:?}", e);
                    previous_frame_end = Box::new(sync::now(self.device.clone())) as Box<_>;
                }
            }

            // Note that in more complex programs it is likely that one of `acquire_next_image`,
            // `command_buffer::submit`, or `present` will block for some time. This happens when the
            // GPU's queue is full and the driver has to wait until the GPU finished some work.
            //
            // Unfortunately the Vulkan API doesn't provide any way to not wait or to detect when a
            // wait would happen. Blocking may be the desired behavior, but if you don't want to
            // block you should spawn a separate thread dedicated to submissions.

            // Handling the window events in order to close the program when the user wants to close
            // it.
            let mut done = false;
            self.events_loop.poll_events(|ev| match ev {
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => done = true,
                Event::WindowEvent {
                    event: WindowEvent::Resized(_),
                    ..
                } => recreate_swapchain = true,
                Event::DeviceEvent {
                    event: DeviceEvent::Key(i),
                    ..
                } => match i {
                    KeyboardInput {
                        virtual_keycode: Some(kc),
                        state: ElementState::Pressed,
                        ..
                    } => {
                        pressed_keys.borrow_mut().insert(kc, true);
                    }
                    KeyboardInput {
                        virtual_keycode: Some(kc),
                        state: ElementState::Released,
                        ..
                    } => {
                        pressed_keys.borrow_mut().insert(kc, false);
                    }
                    _ => {}
                },
                Event::DeviceEvent {
                    event: DeviceEvent::MouseMotion { delta: d },
                    ..
                } => {
                    cam.handle_mouse(d.0 as f32, -d.1 as f32);
                }
                _ => (),
            });

            for (&kc, &p) in pressed_keys.borrow().iter() {
                if p {
                    match kc {
                        VirtualKeyCode::W => cam.move_forward(0.05),
                        VirtualKeyCode::S => cam.move_forward(-0.05),
                        VirtualKeyCode::D => cam.move_sideways(0.05),
                        VirtualKeyCode::A => cam.move_sideways(-0.05),
                        VirtualKeyCode::Space => cam.move_upwards(0.05),
                        VirtualKeyCode::LShift => cam.move_upwards(-0.05),
                        VirtualKeyCode::Escape => {
                            done = true;
                        }
                        _ => {}
                    }
                }
            }

            self.center_cursor();

            if done {
                break;
            }
        }

        Ok(())
    }
}

impl Context for VulkanContext {
    fn get_title(&self) -> String {
        self.win_title.clone()
    }

    fn set_title<T: Into<String> + Clone>(&mut self, t: T) {
        self.win_title = t.clone().into();
        self.surface.window().set_title(t.into().as_str());
    }
}

fn window_size_dependent_setup(
    device: Arc<Device>,
    vs: &vs::Shader,
    fs: &fs::Shader,
    images: &[Arc<SwapchainImage<Window>>],
    render_pass: Arc<dyn RenderPassAbstract + Send + Sync>,
) -> Result<
    (
        Arc<dyn GraphicsPipelineAbstract + Send + Sync>,
        Vec<Arc<dyn FramebufferAbstract + Send + Sync>>,
    ),
    VulkanError,
> {
    let dimensions = images[0].dimensions();

    let depth_buffer = AttachmentImage::transient(device.clone(), dimensions, Format::D16Unorm)?;

    let framebuffers = images
        .iter()
        .map(|image| {
            Arc::new(
                Framebuffer::start(render_pass.clone())
                    .add(image.clone())
                    .unwrap()
                    .add(depth_buffer.clone())
                    .unwrap()
                    .build()
                    .unwrap(),
            ) as Arc<dyn FramebufferAbstract + Send + Sync>
        })
        .collect::<Vec<_>>();

    let pipeline = Arc::new(
        GraphicsPipeline::start()
            .vertex_input_single_buffer::<Vertex>()
            .vertex_shader(vs.main_entry_point(), ())
            .triangle_list()
            .viewports_dynamic_scissors_irrelevant(1)
            .viewports(iter::once(Viewport {
                origin: [0.0, 0.0],
                dimensions: [dimensions[0] as f32, dimensions[1] as f32],
                depth_range: 0.0..1.0,
            }))
            .fragment_shader(fs.main_entry_point(), ())
            .depth_stencil_simple_depth()
            .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
            .cull_mode_back()
            .build(device.clone())?,
    );

    Ok((pipeline, framebuffers))
}
