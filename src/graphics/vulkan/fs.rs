vulkano_shaders::shader! {
    ty: "fragment",
    src: "
#version 450

layout(location = 0) in vec3 v_normal;
layout(location = 1) in vec2 v_tex_coord;
layout(location = 2) in vec3 v_position;
layout(location = 0) out vec4 f_color;

layout(set = 0, binding = 1) uniform sampler2D block_texture;

const vec3 LIGHT_POS = vec3(256.0, 256.0, 0.0);
const vec3 LIGHT_COLOR = vec3(1.0, 1.0, 1.0);
const float AMBIENT = 0.1;

void main() {
    vec3 ambient = AMBIENT * LIGHT_COLOR;

    vec3 light_dir = normalize(LIGHT_POS - v_position);
    float diff = max(dot(normalize(v_normal), light_dir), 0.0);
    vec3 diffuse = diff * LIGHT_COLOR;

    f_color = /*vec4(ambient + diffuse, 1.0) * */texture(block_texture, v_tex_coord);
}
    "
}