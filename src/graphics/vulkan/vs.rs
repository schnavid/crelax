vulkano_shaders::shader! {
    ty: "vertex",
    src: "
#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 tex_coord;

layout(location = 0) out vec3 v_normal;
layout(location = 1) out vec2 v_tex_coord;
layout(location = 2) out vec3 v_position;

layout(set = 0, binding = 0) uniform Data {
    mat4 world;
    mat4 view;
    mat4 proj;
} uniforms;

void main() {
    mat4 worldview = uniforms.view * uniforms.world;
    v_normal = mat3(transpose(inverse(uniforms.world))) * normal;
    v_tex_coord = vec2(tex_coord.x, tex_coord.y);
    v_position = vec3(uniforms.world * vec4(position, 1.0));
    gl_Position = uniforms.proj * worldview * vec4(position.x, position.y, position.z, 1.0);
}
    "
}