pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Color {
    pub fn from_rgba_u8(r: u8, g: u8, b: u8, a: u8) -> Color {
        Color { r, g, b, a }
    }

    pub fn from_rgba_f32(r: f32, g: f32, b: f32, a: f32) -> Color {
        Color {
            r: (r * 256.0) as u8,
            g: (g * 256.0) as u8,
            b: (b * 256.0) as u8,
            a: (a * 256.0) as u8,
        }
    }
}

impl Into<Color> for [f32; 4] {
    fn into(self) -> Color {
        Color::from_rgba_f32(self[0], self[1], self[2], self[3])
    }
}

