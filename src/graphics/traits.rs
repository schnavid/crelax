use crate::world::{World};
use crate::world::registry::BlockRegistry;
use crate::error::Error;

pub trait Context {
    fn get_title(&self) -> String;
    fn set_title<T: Into<String> + Clone>(&mut self, t: T);
}

pub trait ChunkRenderer<C: Context> {
    fn new(context: &C) -> Self;
    fn generate(&mut self, chunk: &World, registry: &BlockRegistry, context: &C, x: i32, y: i32, z: i32) -> Result<(), Error>;
}