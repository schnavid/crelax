use std::collections::HashMap;

pub type TextureId = u32;

#[derive(Debug, Clone)]
pub struct TextureInfo {
    pub module: String,
    pub name: String,
    pub left: u32,
    pub top: u32,
    pub width: u16,
    pub height: u16,
}

pub struct TextureRegistry {
    pub info: Vec<TextureInfo>,
    pub map: HashMap<(String, String), TextureId>,
}

impl TextureRegistry {
    pub fn new() -> TextureRegistry {
        TextureRegistry {
            info: Vec::new(),
            map: HashMap::new(),
        }
    }

    pub fn register(&mut self, info: TextureInfo) -> TextureId {
        let id = self.info.len() as u32;
        self.info.push(info.clone());
        self.map.insert((info.module, info.name), id);
        id
    }

    pub fn get(&self, blk: TextureId) -> Option<TextureInfo> {
        self.info.get(blk as usize).map(|b| b.clone())
    }

    pub fn get_id(&self, module: String, name: String) -> Option<TextureId> {
        self.map.get(&(module, name)).map(|&b| b)
    }
}