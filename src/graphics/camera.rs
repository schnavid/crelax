use cgmath::Matrix4;

pub trait Camera {
    fn matrix(&self) -> Matrix4<f32>;
}

pub trait FirstPersonCamera: Camera {
    fn handle_mouse(&mut self, x: f32, y: f32);
    fn move_forward(&mut self, by: f32);
    fn move_upwards(&mut self, by: f32);
    fn move_sideways(&mut self, by: f32);
}
