pub mod color;
pub mod camera;
pub mod traits;
pub mod vulkan;
pub mod registry;

pub trait GraphicsError {}
