use vulkano::framebuffer::FramebufferCreationError;
use vulkano_win::CreationError;
use vulkano::device::DeviceCreationError;
use vulkano::swapchain::CapabilitiesError;
use vulkano::swapchain::SwapchainCreationError;
use vulkano::command_buffer::DrawError;
use vulkano::command_buffer::DrawIndexedError;
use vulkano::command_buffer::CommandBufferExecError;
use vulkano::command_buffer::BuildError;
use vulkano::image::ImageCreationError;
use vulkano::pipeline::GraphicsPipelineCreationError;
use vulkano::descriptor::descriptor_set::PersistentDescriptorSetError;
use vulkano::sampler::SamplerCreationError;
use vulkano::memory::DeviceMemoryAllocError;
use vulkano::instance::InstanceCreationError;
use crate::world::WorldError;

#[derive(Debug)]
pub enum Error {
    VulkanError(VulkanError),
    WorldError(WorldError),
}

impl From<WorldError> for Error {
    fn from(e: WorldError) -> Self {
        Error::WorldError(e)
    }
}

impl From<VulkanError> for Error {
    fn from(e: VulkanError) -> Self {
        Error::VulkanError(e)
    }
}

#[derive(Debug)]
pub enum VulkanError {
    InstanceCreationError(InstanceCreationError),
    SurfaceCreationError(CreationError),
    DeviceCreationError(DeviceCreationError),
    CapabilitiesError(CapabilitiesError),
    SwapchainCreationError(SwapchainCreationError),
    DrawError(DrawError),
    DrawIndexedError(DrawIndexedError),
    ImageCreationError(ImageCreationError),
    FramebufferCreationError(FramebufferCreationError),
    GraphicsPipelineCreationError(GraphicsPipelineCreationError),
    PersistentDescriptorSetError(PersistentDescriptorSetError),
    SamplerCreationError(SamplerCreationError),
    DeviceMemoryAllocError(DeviceMemoryAllocError),
    CommandBufferExecError(CommandBufferExecError),
    CommandBufferBuildError(BuildError),
    NoPhysicalDeviceAvailable,
    NoGraphicalQueueFamily,
    WindowNoLongerExists,
}

impl From<InstanceCreationError> for VulkanError {
    fn from(e: InstanceCreationError) -> Self {
        VulkanError::InstanceCreationError(e)
    }
}

impl From<CapabilitiesError> for VulkanError {
    fn from(e: CapabilitiesError) -> Self {
        VulkanError::CapabilitiesError(e)
    }
}

impl From<CreationError> for VulkanError {
    fn from(e: CreationError) -> Self {
        VulkanError::SurfaceCreationError(e)
    }
}

impl From<DeviceCreationError> for VulkanError {
    fn from(e: DeviceCreationError) -> Self {
        VulkanError::DeviceCreationError(e)
    }
}

impl From<SwapchainCreationError> for VulkanError {
    fn from(e: SwapchainCreationError) -> Self {
        VulkanError::SwapchainCreationError(e)
    }
}

impl From<DrawError> for VulkanError {
    fn from(e: DrawError) -> Self {
        VulkanError::DrawError(e)
    }
}

impl From<DrawIndexedError> for VulkanError {
    fn from(e: DrawIndexedError) -> Self {
        VulkanError::DrawIndexedError(e)
    }
}

impl From<ImageCreationError> for VulkanError {
    fn from(e: ImageCreationError) -> Self {
        VulkanError::ImageCreationError(e)
    }
}

impl From<FramebufferCreationError> for VulkanError {
    fn from(e: FramebufferCreationError) -> Self {
        VulkanError::FramebufferCreationError(e)
    }
}

impl From<GraphicsPipelineCreationError> for VulkanError {
    fn from(e: GraphicsPipelineCreationError) -> Self {
        VulkanError::GraphicsPipelineCreationError(e)
    }
}

impl From<PersistentDescriptorSetError> for VulkanError {
    fn from(e: PersistentDescriptorSetError) -> Self {
        VulkanError::PersistentDescriptorSetError(e)
    }
}

impl From<SamplerCreationError> for VulkanError {
    fn from(e: SamplerCreationError) -> Self {
        VulkanError::SamplerCreationError(e)
    }
}

impl From<DeviceMemoryAllocError> for VulkanError {
    fn from(e: DeviceMemoryAllocError) -> Self {
        VulkanError::DeviceMemoryAllocError(e)
    }
}

impl From<CommandBufferExecError> for VulkanError {
    fn from(e: CommandBufferExecError) -> Self {
        VulkanError::CommandBufferExecError(e)
    }
}

impl From<BuildError> for VulkanError {
    fn from(e: BuildError) -> Self {
        VulkanError::CommandBufferBuildError(e)
    }
}