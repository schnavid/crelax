extern crate crelax;

use std::net::{TcpListener, TcpStream};
use crelax::protocol::{PacketData, Packet, send_compressed};
use crelax::protocol::traits::{ByteWrite, ByteRead};
use crelax::world::registry::{BlockRegistry, BlockInfo, BlockMaterial, BlockTexturing};
use std::collections::HashMap;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::io::Read;
use std::time::Duration;

fn server_main() -> std::io::Result<()> {
    let listener = TcpListener::bind("127.0.0.1:8080")?;

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("New connection: {}", stream.peer_addr()?);
                handle_connection(stream);
            }
            Err(e) => {
                println!("Unable to connect: {}", e);
            }
        }
    }

    drop(listener);

    Ok(())
}

fn main() {
    match server_main() {
        Ok(_) => println!("Exited sucessfully!"),
        Err(e) => println!("Exited with error: {:?}", e)
    }
}

fn handle_connection(mut stream: TcpStream) {
    std::thread::spawn(move || {
        let packet = PacketData::SendBlockRegistry {
            registry: BlockRegistry {
                info: vec![
                    BlockInfo {
                        module: String::from("crelax"),
                        name: String::from("stone"),
                        material: BlockMaterial::Stone,
                        texture: BlockTexturing::All(0),
                        opaque: true,
                    },
                    BlockInfo {
                        module: String::from("crelax"),
                        name: String::from("nothing"),
                        material: BlockMaterial::Nothing,
                        texture: BlockTexturing::None,
                        opaque: false,
                    }
                ],
                map: HashMap::new(),
            }
        };

        println!("Packet: {:?}", packet);

        send_compressed(packet, &mut stream);

        std::thread::sleep(Duration::from_secs(1));

        let packet = PacketData::PlayerMove {
            x: 1.0,
            y: 2.0,
            z: 3.0,
        };

        println!("Packet: {:?}", packet);

        send_compressed(packet, &mut stream);

        loop {
            match Packet::read_from(&mut stream) {
                Ok(packet) => {
                    println!("{:#?}", packet);
                    match packet {
                        Packet::Uncompressed(PacketData::Disconnect) => break,
                        _ => {}
                    }
                }
                Err(e) => {
                    println!("Connection has been closed!");
                    break;
                }
            }
            println!("LOOP");
        }
    });
}