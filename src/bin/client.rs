extern crate crelax;

use std::io::{Write, Read};
use crelax::protocol::{PacketData, Packet, send_compressed};
use crelax::protocol::traits::{ByteWrite, ByteRead};
use std::net::TcpStream;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;
use crelax::graphics::vulkan::VulkanContext;

fn client_main() -> std::io::Result<()> {
    let (tx, rx) = mpsc::channel();
    let addr = "127.0.0.1:8080";
    let mut stream = TcpStream::connect(addr).unwrap();

    let handle = {
        let mut read_stream = stream.try_clone()?;
        thread::spawn(move || loop {
            match Packet::read_from(&mut read_stream) {
                Ok(packet) => {
//                    println!("{:#?}", packet);
                    tx.send(packet).unwrap();
                }
                Err(e) => {
                    println!("Connection has been closed!");
                    break;
                }
            }
//        thread::sleep(Duration::from_millis(5));
            println!("LOOP");
        })
    };

    let mut graphics_context = VulkanContext::new("crelax", 1920, 1080).unwrap();

    graphics_context.run(rx).unwrap();

    send_compressed(PacketData::Disconnect, &mut stream)?;

    handle.join().unwrap();

    Ok(())
}

fn main() {
    match client_main() {
        Ok(()) => println!("Client exited successfully!"),
        Err(e) => println!("Client exited with error: {:?}", e)
    }
}