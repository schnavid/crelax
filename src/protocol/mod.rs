use crate::protocol::traits::{ByteWrite, ByteRead, WriteChunk, WriteString, WriteBlockTexturing, WriteBool, ReadBool, ReadString, ReadBlockTexturing};
use std::io::{Write, Error, Read, Cursor};
use byteorder::{WriteBytesExt, BigEndian, ReadBytesExt};
use crate::world::Chunk;
use crate::world::registry::{BlockRegistry, BlockInfo};
use flate2::read::ZlibDecoder;
use std::collections::HashMap;
use flate2::write::ZlibEncoder;
use flate2::Compression;
use std::net::TcpStream;

pub mod traits;

#[derive(Debug)]
pub enum Packet {
    Uncompressed(PacketData),
    Compressed(Vec<u8>),
}

impl ByteWrite for Packet {
    fn write_to<W: Write>(&self, to: &mut W) -> Result<(), Error> {
        match self {
            Packet::Uncompressed(data) => {
                to.write_bool(false)?;
                let mut bytes = Vec::new();
                data.write_to(&mut bytes)?;
                to.write_u32::<BigEndian>(bytes.len() as u32)?;
                to.write_all(bytes.as_slice())?;
            }
            Packet::Compressed(data) => {
                to.write_bool(true)?;
                to.write_u32::<BigEndian>(data.len() as u32)?;
                to.write_all(data.as_slice())?;
            }
        }

        Ok(())
    }
}

impl ByteRead for Packet {
    fn read_from<R: Read>(from: &mut R) -> Result<Self, Error> {
        let compressed = from.read_bool()?;
        let len = from.read_u32::<BigEndian>()?;

        let mut buffer = vec![0u8; len as usize];

        from.read_exact(&mut buffer)?;

        if compressed {
            let mut decoder = ZlibDecoder::new(Cursor::new(buffer));
            let data = PacketData::read_from(&mut decoder)?;
            Ok(Packet::Uncompressed(data))
        } else {
            let data = PacketData::read_from(&mut Cursor::new(buffer))?;
            Ok(Packet::Uncompressed(data))
        }
    }
}

#[derive(Debug)]
pub enum PacketData {
    // TO SERVER
    PlayerMove {
        x: f64,
        y: f64,
        z: f64,
    },
    Disconnect,

    // TO CLIENT
    ChunkUpdate {
        x: i32,
        y: i32,
        z: i32,
        blocks: Chunk,
    },
    SendBlockRegistry {
        registry: BlockRegistry
    },
}

impl ByteWrite for PacketData {
    fn write_to<W: Write>(&self, to: &mut W) -> std::io::Result<()> {
        to.write_u16::<BigEndian>(packet_content_type(self))?;

        match self {
            PacketData::PlayerMove { x, y, z } => {
                to.write_f64::<BigEndian>(*x)?;
                to.write_f64::<BigEndian>(*y)?;
                to.write_f64::<BigEndian>(*z)?;
            }
            PacketData::ChunkUpdate { x, y, z, blocks } => {
                to.write_i32::<BigEndian>(*x)?;
                to.write_i32::<BigEndian>(*y)?;
                to.write_i32::<BigEndian>(*z)?;
                to.write_chunk(blocks)?;
            }
            PacketData::SendBlockRegistry { registry } => {
                to.write_u32::<BigEndian>(registry.info.len() as u32)?;
                for info in &registry.info {
                    to.write_string(&info.module)?;
                    to.write_string(&info.name)?;
                    to.write_u8(info.material.clone().into())?;
                    to.write_block_texturing(info.texture.clone())?;
                    to.write_bool(info.opaque)?;
                }
            }
            PacketData::Disconnect => {}
        }

        Ok(())
    }
}

impl ByteRead for PacketData {
    fn read_from<R: Read>(from: &mut R) -> Result<Self, Error> {
        let kind = from.read_u16::<BigEndian>()?;

        match kind {
            0 => {
                let x = from.read_f64::<BigEndian>()?;
                let y = from.read_f64::<BigEndian>()?;
                let z = from.read_f64::<BigEndian>()?;

                Ok(PacketData::PlayerMove {
                    x,
                    y,
                    z,
                })
            }
            2 => {
                let len = from.read_u32::<BigEndian>()?;

                let mut info = Vec::new();
                info.reserve(len as usize);
                let mut map = HashMap::new();

                for i in 0..len {
                    let id = info.len();

                    let module = from.read_string()?;
                    let name = from.read_string()?;
                    let material = from.read_u8()?.into();
                    let texture = from.read_block_texturing()?;
                    let opaque = from.read_bool()?;

                    info.push(
                        BlockInfo {
                            module: module.clone(),
                            name: name.clone(),
                            material,
                            texture,
                            opaque,
                        }
                    );

                    map.insert((module, name), id as u32);
                }

                Ok(
                    PacketData::SendBlockRegistry {
                        registry: BlockRegistry {
                            info,
                            map,
                        }
                    }
                )
            }
            3 => Ok(PacketData::Disconnect),
            _ => Err(Error::from(std::io::ErrorKind::InvalidData))
        }
    }
}

fn packet_content_type(x: &PacketData) -> u16 {
    match x {
        // TO SERVER
        PacketData::PlayerMove { .. } => 0,
        PacketData::Disconnect => 3,
        // TO CLIENT
        PacketData::ChunkUpdate { .. } => 1,
        PacketData::SendBlockRegistry { .. } => 2,
    }
}

pub fn send_uncompressed(packet: PacketData, stream: &mut TcpStream) -> std::io::Result<()> {
    let packet = Packet::Uncompressed(packet);
    packet.write_to(stream)
}

pub fn send_compressed(packet: PacketData, stream: &mut TcpStream) -> std::io::Result<()> {
    let mut encoder = ZlibEncoder::new(Vec::new(), Compression::best());

    packet.write_to(&mut encoder)?;

    let packet = Packet::Compressed(encoder.finish()?);

    packet.write_to(stream)
}