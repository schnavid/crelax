use std::io::{Write, Read};
use crate::world::Chunk;
use byteorder::{WriteBytesExt, BigEndian, ReadBytesExt};
use crate::world::registry::BlockTexturing;

pub trait ByteWrite {
    fn write_to<W: Write>(&self, to: &mut W) -> std::io::Result<()>;
}

pub trait ByteRead: Sized {
    fn read_from<R: Read>(from: &mut R) -> std::io::Result<Self>;
}

pub trait WriteChunk: std::io::Write {
    fn write_chunk(&mut self, chunk: &Chunk) -> std::io::Result<()> {
        for b in chunk.blocks.0.iter() {
            self.write_u32::<BigEndian>(*b)?;
        }

        Ok(())
    }
}

impl<W: Write> WriteChunk for W {}

pub trait WriteString: std::io::Write {
    fn write_string(&mut self, string: &String) -> std::io::Result<()> {
        self.write_u8(string.len() as u8)?;
        self.write_all(string.as_bytes())?;

        Ok(())
    }
}

impl<W: Write> WriteString for W {}

pub trait ReadString: std::io::Read {
    fn read_string(&mut self) -> std::io::Result<String> {
        let len = self.read_u8()?;

        let mut buf = vec![0u8; len as usize];
        self.read_exact(&mut buf)?;

        Ok(String::from_utf8(buf).unwrap())
    }
}

impl<R: Read> ReadString for R {}

pub trait WriteBlockTexturing: std::io::Write {
    fn write_block_texturing(&mut self, t: BlockTexturing) -> std::io::Result<()> {
        match t {
            BlockTexturing::All(t) => {
                self.write_u8(0)?;
                self.write_u32::<BigEndian>(t)?;
            }
            BlockTexturing::Pillar { top, side, bottom } => {
                self.write_u8(1)?;
                self.write_u32::<BigEndian>(top)?;
                self.write_u32::<BigEndian>(side)?;
                self.write_u32::<BigEndian>(bottom)?;
            }
            BlockTexturing::None => {
                self.write_u8(2)?;
            }
        }

        Ok(())
    }
}

impl<W: Write> WriteBlockTexturing for W {}

pub trait ReadBlockTexturing: std::io::Read {
    fn read_block_texturing(&mut self) -> std::io::Result<BlockTexturing> {
        let kind = self.read_u8()?;

        match kind {
            0 => {
                Ok(BlockTexturing::All(self.read_u32::<BigEndian>()?))
            }
            1 => {
                let top = self.read_u32::<BigEndian>()?;
                let side = self.read_u32::<BigEndian>()?;
                let bottom = self.read_u32::<BigEndian>()?;

                Ok(BlockTexturing::Pillar { top, side, bottom })
            }
            2 => Ok(BlockTexturing::None),
            _ => Err(std::io::Error::from(std::io::ErrorKind::InvalidData))
        }
    }
}

impl<R: Read> ReadBlockTexturing for R {}

pub trait WriteBool: std::io::Write {
    fn write_bool(&mut self, b: bool) -> std::io::Result<()> {
        self.write_u8(if b { 1 } else { 0 })?;

        Ok(())
    }
}

impl<W: Write> WriteBool for W {}

pub trait ReadBool: std::io::Read {
    fn read_bool(&mut self) -> std::io::Result<bool> {
        let b = self.read_u8()?;

        if b == 1 {
            Ok(true)
        } else if b == 0 {
            Ok(false)
        } else {
            Err(std::io::Error::from(std::io::ErrorKind::InvalidData))
        }
    }
}

impl<R: Read> ReadBool for R {}